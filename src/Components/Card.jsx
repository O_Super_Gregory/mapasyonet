/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

const Card = ({
  children,
  className,
}) => (
  <div className={`card ${className}`}>
    <div className="card-content">
      {children}
    </div>
  </div>
);

Card.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

Card.defaultProps = {
  className: '',
  children: {},
};

export default Card;
