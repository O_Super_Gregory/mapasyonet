import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
  className,
  label,
  value,
  type,
  onChange,
  name,
}) => (
  <label className={`input-field ${className}`} htmlFor="input">
    {label}
    <input
      value={value}
      type={type}
      onChange={onChange}
      name={name}
      id="input"
    />
  </label>
);

Input.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

Input.defaultProps = {
  className: '',
  label: '',
  type: 'text',
  onChange: undefined,
  name: '',
  value: '',
};

export default Input;
