import React from 'react';
import Enzyme from 'enzyme';
import { MapContainer } from '../MapContainer';

describe('<MapContainer />', () => {
  const mapContainer = Enzyme.shallow(<MapContainer google={{}} />);

  const { addMarker } = mapContainer.instance();

  beforeEach(() => {
    mapContainer.setState({ markers: [], activeMarker: null });
  });

  it('should map state to Markers', () => {
    addMarker('', { lat: 0, lng: 0 });
    addMarker('', { lat: 0, lng: 0 });

    expect(mapContainer.find('Marker').length).toBe(2);
  });

  it('should show marker`s text in InfoWindow on marker click', () => {
    addMarker('asdsa', { lat: 0, lng: 0 });
    const marker = mapContainer.find('Marker');
    marker.simulate('click', null, { name: marker.prop('name') });

    expect(mapContainer.find('InfoWindow').prop('visible')).toBeTruthy();
    expect(mapContainer.find('InfoWindow > div > h3').text()).toEqual('asdsa');
  });
});
