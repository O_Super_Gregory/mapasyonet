import React from 'react';
import Enzyme from 'enzyme';

import AddMarkerForm from '../AddMarkerForm';

describe('<AddMarkerForm />', () => {
  const fakeAddMarker = jest.fn();
  const addMarkerForm = Enzyme.shallow(<AddMarkerForm addMarker={fakeAddMarker} />);

  it('should render correctly', () => {
    expect(addMarkerForm).toMatchSnapshot();
  });

  const addValueToInputs = () => {
    addMarkerForm.find('Input')
      .forEach((input) => {
        const name = input.prop('name').valueOf();
        input
          .simulate('change', { target: { name, value: name } });
      });
  };

  it('all inputs should have value to submit button be active', () => {
    expect(addMarkerForm.find('button').prop('disabled')).toBe(true);

    addValueToInputs();

    expect(addMarkerForm.find('button').prop('disabled')).toBe(false);
  });

  it('should call addMarker func with inputs values on add button click', () => {
    addValueToInputs();

    addMarkerForm
      .find('button')
      .prop('onClick')();

    expect(fakeAddMarker)
      .toBeCalledWith('text', { lat: 'lat', lng: 'lng' });
  });
});
