import React from 'react';
import PropTypes from 'prop-types';
import Input from '../Components/Input';
import Card from '../Components/Card';

class AddMarkerForm extends React.PureComponent {
  state = {
    lat: '',
    lng: '',
    text: '',
  }

  handleChange = ({ target }) => {
    const { name, value } = target;
    this.setState({ [name]: value });
  };

  submitMarker = () => {
    const { lat, lng, text } = this.state;
    const { addMarker } = this.props;
    addMarker(text, { lat, lng });
  };

  canSubmit() {
    const { lat, lng, text } = this.state;
    return (!lat || !lng || !text);
  }

  render() {
    const { lat, lng, text } = this.state;
    const isButtonDisabled = this.canSubmit();

    return (
      <div className="container">
        <div className="row">

          <Card className="col s12 m5">

            <h5>Adicionar Marcador</h5>

            <Input
              label="Texto"
              name="text"
              value={text}
              onChange={this.handleChange}
              className="s12"
            />
            <Input
              label="Latitude"
              name="lat"
              value={lat}
              onChange={this.handleChange}
              type="number"
              className="col s6"
            />
            <Input
              label="Longitude"
              name="lng"
              value={lng}
              onChange={this.handleChange}
              type="number"
              className="col s6"
            />

            <button
              type="button"
              disabled={isButtonDisabled}
              onClick={this.submitMarker}
              className="row right btn-floating btn-large waves-effect waves-light red"
            >
              <i className="material-icons">add</i>
            </button>
          </Card>

        </div>
      </div>
    );
  }
}

AddMarkerForm.propTypes = {
  addMarker: PropTypes.func.isRequired,
};

export default AddMarkerForm;
