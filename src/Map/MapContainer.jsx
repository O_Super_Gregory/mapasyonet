import React from 'react';
import
{
  Map,
  Marker,
  InfoWindow,
  GoogleApiWrapper,
} from 'google-maps-react';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';

import AddMarkerForm from './AddMarkerForm';

export class MapContainer extends React.Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    google: PropTypes.object.isRequired,
  }

  state = {
    activeMarker: {},
    markers: [],
  }

  setActiveMarker = (marker) => {
    this.setState({ activeMarker: marker });
  }

  addMarker = (text, { lat, lng }) => {
    this.setState(prev => ({
      markers: [
        ...prev.markers,
        {
          text,
          position: { lat, lng },
        },
      ],
    }));
  }

  render() {
    const { google } = this.props;
    const { activeMarker, markers } = this.state;
    return (
      <Map
        google={google}
        onClick={() => this.setActiveMarker(null)}
        centerAroundCurrentLocation
        zoom={8}
      >
        <AddMarkerForm addMarker={this.addMarker} />

        {markers.map(marker => (
          <Marker
            key={uuidv4()}
            onClick={(p, mapMarker) => this.setActiveMarker(mapMarker)}
            name={marker.text}
            position={marker.position}
          />
        ))}

        <InfoWindow
          marker={activeMarker}
          visible={!!activeMarker}
          onClose={() => this.setActiveMarker(null)}
        >
          <div>
            <h3>{activeMarker && activeMarker.name}</h3>
          </div>
        </InfoWindow>
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: (process.env.REACT_APP_GOOGLE_API_KEY),
})(MapContainer);
