import React from 'react';
import MapContainer from './Map/MapContainer';

const App = () => <MapContainer />;

export default App;
