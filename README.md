# MapaSyonet

Inicie com ```npm start```  
Coloque a key da api do google maps na variavel de ambiente *REACT_APP_GOOGLE_API_KEY*  
Tipo:  
- Windows: ```$env:REACT_APP_GOOGLE_API_KEY="AQUI" npm start```  
- Unix: ```REACT_APP_GOOGLE_API_KEY=AQUI npm start```  

Testes: ```npm test```  

Componente: ```<MapContainer />```